from multiprocessing import Pool
from django.contrib import admin
from .models import Poll, PollAnswer


admin.site.register(Poll)
admin.site.register(PollAnswer)
