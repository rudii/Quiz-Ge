from django.db import models
# from django.contrib.postgres.fields import ArrayField

class Poll(models.Model):
    question = models.CharField(max_length=150)
    # answers = ArrayField(models.CharField(max_length=200), blank=True)

    def __str__(self):
        return self.question

class PollAnswer(models.Model):
    question_id = models.ForeignKey(Poll, on_delete=models.CASCADE)
    answer = models.TextField()
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.question_id.question + " | " + self.answer