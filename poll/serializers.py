from rest_framework import serializers
from .models import Poll, PollAnswer

class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
        fields = ['id', 'question']


class PollAnswersSerializer(serializers.ModelSerializer):
    class Meta:
        model = PollAnswer
        fields = ['id', 'answer', "votes"]