import http
import json
from django.http import JsonResponse
from .models import Poll, PollAnswer
from .serializers import PollAnswersSerializer, PollSerializer
from rest_framework.decorators import api_view
from rest_framework.response import Response;
from rest_framework import status
from django.shortcuts import redirect
from poll import serializers;

@api_view(['GET', 'POST'])
def poll_list(request):

    if request.method == "GET":
        polls = Poll.objects.all()
        serializer =  PollSerializer(polls, many=True)
        return Response({"polls": serializer.data})
    if request.method == "POST":
        serializer = PollSerializer(data=request.data)
        if serializer.is_valid():
            j = serializer.save()
            return redirect('/api/v1/poll/'+str(j.pk))

@api_view(['GET', 'POST'])
def poll_detail(request, id):
    if request.method == "GET":
        try:
            quest = Poll.objects.get(pk=id)
        except Poll.DoesNotExist:
            return Response(status=status.HTTP_404_NOT_FOUND)
        poll = PollAnswer.objects.filter(question_id=id)
        

        serializers = PollAnswersSerializer(poll, many=True)
        a = json.dumps({"question":{"id": id, "content":"??"}, "answers":serializers.data})
        b = json.loads(a)
        b["question"].update({"content":quest.question})

        return Response(b)


    if request.method == "POST":
        serializer = PollAnswersSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)


@api_view(['GET'])
def poll_vote(request, answer_id):

    try:
        selected_choice = PollAnswer.objects.get(pk=answer_id)
    except:
        return Response(status=status.HTTP_404_NOT_FOUND)
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return redirect('/api/v1/poll/'+str(selected_choice.question_id.pk))
